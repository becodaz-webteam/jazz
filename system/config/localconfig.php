<?php

### INSTALL SCRIPT START ###
$GLOBALS['TL_CONFIG']['licenseAccepted'] = true;
$GLOBALS['TL_CONFIG']['installPassword'] = '$2y$10$/FuqV5egkQu45fBPLAWMZeja9.QeaiAdaujCPqFR53dRmNYDUJM92';
$GLOBALS['TL_CONFIG']['adminEmail'] = 'office@becoda.at';
$GLOBALS['TL_CONFIG']['exampleWebsite'] = 1519902906;
$GLOBALS['TL_CONFIG']['folderUrl'] = true;
### INSTALL SCRIPT STOP ###
