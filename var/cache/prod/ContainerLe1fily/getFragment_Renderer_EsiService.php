<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'fragment.renderer.esi' shared service.

$this->services['fragment.renderer.esi'] = $instance = new \Symfony\Component\HttpKernel\Fragment\EsiFragmentRenderer(${($_ = isset($this->services['esi']) ? $this->services['esi'] : $this->services['esi'] = new \Symfony\Component\HttpKernel\HttpCache\Esi()) && false ?: '_'}, ${($_ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->load(__DIR__.'/getFragment_Renderer_InlineService.php')) && false ?: '_'}, ${($_ = isset($this->services['uri_signer']) ? $this->services['uri_signer'] : $this->services['uri_signer'] = new \Symfony\Component\HttpKernel\UriSigner('5bc32f52247798d879a8d4b7b4ad388d1943abd5a6c07bf7e6e83f4dc1093bfd')) && false ?: '_'});

$instance->setFragmentPath('/_fragment');

return $instance;
