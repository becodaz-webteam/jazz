<?php

namespace  {
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2018 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Table tl_newsletter_blacklist
 */
$GLOBALS['TL_DCA']['tl_newsletter_blacklist'] = array
(

	// Config
	'config' => array
	(
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
                'pid' => 'index',
				'hash' => 'unique'
			)
		)
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
		),
		'hash' => array
		(
			'sql'                     => "varchar(32) NULL"
		)
	)
);
}
