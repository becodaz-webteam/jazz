<?php

/* ContaoCoreBundle:Error:invalid_request_token.html.twig */
class __TwigTemplate_4d6b9f5ee42d5260800a02ce4bfdf6dd52ec43ba26956c18f3212cf4a2ddf2a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ContaoCore/Error/layout.html.twig", "ContaoCoreBundle:Error:invalid_request_token.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'matter' => array($this, 'block_matter'),
            'howToFix' => array($this, 'block_howToFix'),
            'explain' => array($this, 'block_explain'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ContaoCore/Error/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? null), "requestToken", array()), "html", null, true);
        echo "
";
    }

    // line 5
    public function block_matter($context, array $blocks = array())
    {
        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? null), "invalidToken", array()), "html", null, true);
        echo "</p>
";
    }

    // line 8
    public function block_howToFix($context, array $blocks = array())
    {
        // line 9
        echo "    <p>";
        echo $this->getAttribute(($context["error"] ?? null), "tokenRetry", array());
        echo "</p>
";
    }

    // line 11
    public function block_explain($context, array $blocks = array())
    {
        // line 12
        echo "    <p>";
        echo $this->getAttribute(($context["error"] ?? null), "tokenExplainOne", array());
        echo "</p>
    <p>";
        // line 13
        echo $this->getAttribute(($context["error"] ?? null), "tokenExplainTwo", array());
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "ContaoCoreBundle:Error:invalid_request_token.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  64 => 12,  61 => 11,  54 => 9,  51 => 8,  44 => 6,  41 => 5,  34 => 3,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ContaoCoreBundle:Error:invalid_request_token.html.twig", "/var/www/vhosts/hosting106358.a2f24.netcup.net/httpdocs/becoda.at/dev/contao4/jazz/vendor/contao/core-bundle/src/Resources/views/Error/invalid_request_token.html.twig");
    }
}
