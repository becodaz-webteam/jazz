<?php

/* ContaoCoreBundle:Error:no_layout_specified.html.twig */
class __TwigTemplate_4c585437089ed304ba2b93f26852044abefe5c1f62a6b8b3399182466aabb3f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ContaoCore/Error/layout.html.twig", "ContaoCoreBundle:Error:no_layout_specified.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'matter' => array($this, 'block_matter'),
            'howToFix' => array($this, 'block_howToFix'),
            'explain' => array($this, 'block_explain'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ContaoCore/Error/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? null), "noLayout", array()), "html", null, true);
        echo "
";
    }

    // line 5
    public function block_matter($context, array $blocks = array())
    {
        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? null), "noLayoutSpecified", array()), "html", null, true);
        echo "</p>
";
    }

    // line 8
    public function block_howToFix($context, array $blocks = array())
    {
        // line 9
        echo "    <p>";
        echo sprintf($this->getAttribute(($context["error"] ?? null), "noLayoutFix", array()), ($context["adminEmail"] ?? null));
        echo "</p>
";
    }

    // line 11
    public function block_explain($context, array $blocks = array())
    {
        // line 12
        echo "    <p>";
        echo $this->getAttribute(($context["error"] ?? null), "noLayoutExplain", array());
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "ContaoCoreBundle:Error:no_layout_specified.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 12,  61 => 11,  54 => 9,  51 => 8,  44 => 6,  41 => 5,  34 => 3,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ContaoCoreBundle:Error:no_layout_specified.html.twig", "/var/www/vhosts/hosting106358.a2f24.netcup.net/httpdocs/becoda.at/dev/contao4/jazz/vendor/contao/core-bundle/src/Resources/views/Error/no_layout_specified.html.twig");
    }
}
