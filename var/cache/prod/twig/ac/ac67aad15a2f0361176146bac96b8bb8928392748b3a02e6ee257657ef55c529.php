<?php

/* ContaoCoreBundle:Error:no_root_page_found.html.twig */
class __TwigTemplate_85ed97d838fbe79fa127102aec32415c8a88d9d18b05516fd0a40af5d1fc55b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ContaoCore/Error/layout.html.twig", "ContaoCoreBundle:Error:no_root_page_found.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'matter' => array($this, 'block_matter'),
            'howToFix' => array($this, 'block_howToFix'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ContaoCore/Error/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? null), "noRoot", array()), "html", null, true);
        echo "
";
    }

    // line 5
    public function block_matter($context, array $blocks = array())
    {
        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? null), "noRootPage", array()), "html", null, true);
        echo "</p>
";
    }

    // line 8
    public function block_howToFix($context, array $blocks = array())
    {
        // line 9
        echo "    <p>";
        echo sprintf($this->getAttribute(($context["error"] ?? null), "noRootFix", array()), ($context["base"] ?? null), ($context["adminEmail"] ?? null));
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "ContaoCoreBundle:Error:no_root_page_found.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 9,  50 => 8,  43 => 6,  40 => 5,  33 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ContaoCoreBundle:Error:no_root_page_found.html.twig", "/var/www/vhosts/hosting106358.a2f24.netcup.net/httpdocs/becoda.at/dev/contao4/jazz/vendor/contao/core-bundle/src/Resources/views/Error/no_root_page_found.html.twig");
    }
}
