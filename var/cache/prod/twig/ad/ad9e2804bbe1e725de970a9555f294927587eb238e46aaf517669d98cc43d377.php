<?php

/* ContaoInstallationBundle::database.html.twig */
class __TwigTemplate_4c4f0f9f8a03af324e925252a413ba44ccdf00c9d04c5f9670e0ef05a46755ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ContaoInstallation/layout.html.twig", "ContaoInstallationBundle::database.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ContaoInstallation/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "  <fieldset class=\"tl_tbox nolegend\">
    <div>
      <h3>";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_connection"), "html", null, true);
        echo "</h3>
      ";
        // line 7
        if (array_key_exists("database_error", $context)) {
            // line 8
            echo "        <p class=\"tl_error\">";
            echo twig_escape_filter($this->env, ($context["database_error"] ?? null), "html", null, true);
            echo "</p>
      ";
        }
        // line 10
        echo "      <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_connection_explain"), "html", null, true);
        echo "</p>
      <form method=\"post\">
        <div class=\"tl_formbody\">
          <input type=\"hidden\" name=\"FORM_SUBMIT\" value=\"tl_database_login\">
          <input type=\"hidden\" name=\"REQUEST_TOKEN\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["request_token"] ?? null), "html", null, true);
        echo "\">
          <h4><label for=\"dbHost\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_host"), "html", null, true);
        echo "</label></h4>
          <input type=\"text\" name=\"dbHost\" id=\"dbHost\" class=\"tl_text\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["parameters"] ?? null), "database_host", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["parameters"] ?? null), "database_host", array()), "localhost")) : ("localhost")), "html", null, true);
        echo "\">
          <h4><label for=\"dbPort\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_port"), "html", null, true);
        echo "</label></h4>
          <input type=\"text\" name=\"dbPort\" id=\"dbPort\" class=\"tl_text\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["parameters"] ?? null), "database_port", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["parameters"] ?? null), "database_port", array()), 3306)) : (3306)), "html", null, true);
        echo "\">
          <h4><label for=\"dbUser\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_user"), "html", null, true);
        echo "</label></h4>
          <input type=\"text\" name=\"dbUser\" id=\"dbUser\" class=\"tl_text\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["parameters"] ?? null), "database_user", array()), "html", null, true);
        echo "\">
          <h4><label for=\"dbPassword\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_password"), "html", null, true);
        echo "</label></h4>
          <input type=\"password\" name=\"dbPassword\" id=\"dbPassword\" class=\"tl_text\" value=\"";
        // line 22
        if ($this->getAttribute(($context["parameters"] ?? null), "database_password", array())) {
            echo "*****";
        }
        echo "\">
          <h4><label for=\"dbName\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("database_name"), "html", null, true);
        echo "</label></h4>
          <input type=\"text\" name=\"dbName\" id=\"dbName\" class=\"tl_text\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["parameters"] ?? null), "database_name", array()), "html", null, true);
        echo "\">
        </div>
        <div class=\"tl_formbody_submit\">
          <div class=\"tl_submit_container\">
            <button type=\"submit\" class=\"tl_submit\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("save_settings"), "html", null, true);
        echo "</button>
          </div>
        </div>
      </form>
    </div>
  </fieldset>
  <script>
    \$('dbHost').focus();
  </script>
";
    }

    public function getTemplateName()
    {
        return "ContaoInstallationBundle::database.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 28,  97 => 24,  93 => 23,  87 => 22,  83 => 21,  79 => 20,  75 => 19,  71 => 18,  67 => 17,  63 => 16,  59 => 15,  55 => 14,  47 => 10,  41 => 8,  39 => 7,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ContaoInstallationBundle::database.html.twig", "/var/www/vhosts/hosting106358.a2f24.netcup.net/httpdocs/becoda.at/dev/contao4/jazz/vendor/contao/installation-bundle/src/Resources/views/database.html.twig");
    }
}
