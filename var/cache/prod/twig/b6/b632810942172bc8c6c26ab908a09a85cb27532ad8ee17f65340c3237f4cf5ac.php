<?php

/* ContaoCoreBundle:Error:error.html.twig */
class __TwigTemplate_34489c680e03abf91444f653cb187df02889e0cf211577026962926efe5a0c47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ContaoCore/Error/layout.html.twig", "ContaoCoreBundle:Error:error.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'matter' => array($this, 'block_matter'),
            'howToFix' => array($this, 'block_howToFix'),
            'explain' => array($this, 'block_explain'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ContaoCore/Error/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["error"] ?? null), "error", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["error"] ?? null), "error", array()), "An error occurred")) : ("An error occurred")), "html", null, true);
        echo "
";
    }

    // line 5
    public function block_matter($context, array $blocks = array())
    {
        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["error"] ?? null), "errorOccurred", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["error"] ?? null), "errorOccurred", array()), "An error occurred while executing this script. Something does not work properly. Additionally an error occurred while trying to display the error message.")) : ("An error occurred while executing this script. Something does not work properly. Additionally an error occurred while trying to display the error message.")), "html", null, true);
        echo "</p>
";
    }

    // line 8
    public function block_howToFix($context, array $blocks = array())
    {
        // line 9
        echo "    <p>";
        echo (($this->getAttribute(($context["error"] ?? null), "errorFixOne", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["error"] ?? null), "errorFixOne", array()), "Open the current log file in the <code>var/logs</code> or <code>app/logs</code> directory and find the associated error message (usually the last one).")) : ("Open the current log file in the <code>var/logs</code> or <code>app/logs</code> directory and find the associated error message (usually the last one)."));
        echo "</p>
";
    }

    // line 11
    public function block_explain($context, array $blocks = array())
    {
        // line 12
        echo "    <p>";
        echo (($this->getAttribute(($context["error"] ?? null), "errorExplain", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["error"] ?? null), "errorExplain", array()), "The script execution stopped, because something does not work properly. The actual error message is hidden by this notice for security reasons and can be found in the current log file (see above). If you do not understand the error message or do not know how to fix the problem, search the <a href=\"https://contao.org/faq.html\">Contao FAQs</a> or visit the <a href=\"https://contao.org/support.html\">Contao support page</a>.")) : ("The script execution stopped, because something does not work properly. The actual error message is hidden by this notice for security reasons and can be found in the current log file (see above). If you do not understand the error message or do not know how to fix the problem, search the <a href=\"https://contao.org/faq.html\">Contao FAQs</a> or visit the <a href=\"https://contao.org/support.html\">Contao support page</a>."));
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "ContaoCoreBundle:Error:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 12,  61 => 11,  54 => 9,  51 => 8,  44 => 6,  41 => 5,  34 => 3,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ContaoCoreBundle:Error:error.html.twig", "/var/www/vhosts/hosting106358.a2f24.netcup.net/httpdocs/becoda.at/dev/contao4/jazz/vendor/contao/core-bundle/src/Resources/views/Error/error.html.twig");
    }
}
