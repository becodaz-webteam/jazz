<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'SimplePie' => array($vendorDir . '/simplepie/simplepie/library'),
    'SensioLabs\\Security' => array($vendorDir . '/sensiolabs/security-checker'),
    'SensioLabs\\AnsiConverter' => array($vendorDir . '/sensiolabs/ansi-to-html'),
    'Pdp\\' => array($vendorDir . '/jeremykendall/php-domain-parser/src'),
    'Less' => array($vendorDir . '/oyejorge/less.php/lib'),
    'Knp\\Bundle\\TimeBundle' => array($vendorDir . '/knplabs/knp-time-bundle'),
    'Imagine' => array($vendorDir . '/imagine/imagine/lib'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
);
