# Contao comments bundle change log

### 4.4.10 (2017-12-27)

 * Add the missing options array in the CommentsModel class (see #9).

### 4.4.0-RC2 (2017-06-12)

 * Trigger all the callbacks in the toggleVisibility() methods (see contao/core-bundle#756).

### 4.4.0-RC1 (2017-05-23)

 * Notify subscribers of replies (see contao/core#8565).

### 4.4.0-beta1 (2017-05-05)

 * Optimize the element preview height (see contao/core-bundle#678).
