<?php

/*
 * This file is part of Contao.
 *
 * Copyright (c) 2005-2018 Leo Feyer
 *
 * @license LGPL-3.0+
 */

namespace Contao\CoreBundle\Exception;

/**
 * Incomplete installation exception.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class IncompleteInstallationException extends \RuntimeException
{
}
