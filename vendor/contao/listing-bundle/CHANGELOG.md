# Contao listing bundle change log

### 4.4.8 (2017-11-15)

 * Prevent SQL injections in the listing module (see CVE-2017-16558).

### 4.4.0-RC1 (2017-05-23)

 * Show a "no results" notice if a search does not return any results (see contao/core#7705).
