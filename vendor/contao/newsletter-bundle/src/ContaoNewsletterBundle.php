<?php

/*
 * This file is part of Contao.
 *
 * Copyright (c) 2005-2018 Leo Feyer
 *
 * @license LGPL-3.0+
 */

namespace Contao\NewsletterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao newsletter bundle.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class ContaoNewsletterBundle extends Bundle
{
}
